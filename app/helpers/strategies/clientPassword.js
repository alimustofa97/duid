/**
 * Created by faerulsalamun on 2/16/16.
 */

'use strict';

const passport = require('passport');
const Promise = require('bluebird');
const ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy;
const Async = require('../../services/Async');

// Models
const OAuthClient = require('../../models/OAuthClient');

function wrap(genFunction) {
    const coroutine = Promise.coroutine(genFunction);

    return (clientId, clientSecret, done) => {
        return coroutine(clientId, clientSecret, done).catch(done);
    };
}

const handleAuth = wrap(function *(clientId, clientSecret, done) {
    const client = yield OAuthClient.findOne({ _id: clientId });

    if (!client) return done(null, false);
    if (!client.trusted) return done(null, false);

    if (client.secret == clientSecret) return done(null, client);
    return done(null, false);
});

const strategy = new ClientPasswordStrategy(handleAuth);
passport.use('clientPassword', strategy);

// passport.use('clientPassword', new ClientPasswordStrategy(
//     (clientId, clientSecret, done) => {
//         OAuthClient.findOne({_id: clientId}, (err, clientApp)  => {

//             if (err) return done(err);
//             if (!clientApp) return done(null, false);
//             if (!clientApp.trusted) return done(null, false);

//             if (clientApp.secret == clientSecret) return done(null, clientApp);
//             else return done(null, false);
//         });
//     }
// ));
