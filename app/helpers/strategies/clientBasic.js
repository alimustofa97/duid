/**
 * Created by faerulsalamun on 2/16/16.
 */

'use strict';

const passport = require('passport');
const BasicStrategy = require('passport-http').BasicStrategy;
const Async = require('../../services/Async');

// Models
const OAuthClient = require('../../models/OAuthClient');

/**
 * This strategy is used to authenticate registered OAuth clients.
 * The authentication data must be delivered using the basic authentication scheme.
 */

const handleAuth = Async.make(function *(clientId, clientSecret, done) {
    const client = yield OAuthClient.findOne({ _id: clientId });

    if (!client) return done(null, false);
    if (!client.trusted) return done(null, false);

    if (client.secret == clientSecret) return done(null, client);
    return done(null, false);
});

const strategy = new BasicStrategy(handleAuth);
passport.use('clientBasic', strategy);

// passport.use('clientBasic', new BasicStrategy((clientId, clientSecret, done) => {

//     OAuthClient.findOne({ _id: clientId }, (err, clientApp) => {

//         if (err) return done(err);
//         if (!clientApp) return done(null, false);
//         if (!clientApp.trusted) return done(null, false);

//         if (clientApp.secret == clientSecret) return done(null, clientApp);
//         else return done(null, false);
//     });
// }));
