/**
 * Created by faerulsalamun on 2/18/16.
 */

'use strict';

const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const path = require('path');
const config = require('../../config/' + (process.env.NODE_ENV || ''));

// Services
const Async = require('../services/Async');
const Utils = require('../services/Utils');

module.exports = {

    base64: Async.make(function *(prefix, dir, base64string) {
        const trimmed = base64string.trim();
        const validationRe = /^data\:image\/(png|jpeg|gif);base64,/;

        if (!validationRe.test(trimmed)) {
            throw error('Not a valid image type', 400);
        }

        const patternMatchingResult = trimmed.match(validationRe);
        if (!patternMatchingResult) { // less likely to occur
            throw error('Not a valid image type', 400);
        }

        const ext = patternMatchingResult[1] === 'jpeg' ? 'jpg' : patternMatchingResult[1];
        const newFilename = `${prefix}_${Utils.uid(32)}_${Date.now()}.${ext}`;
        const newBase64String = trimmed.replace(validationRe, '');
        const newPath = path.join(config.uploadDir, dir, newFilename);

        const dirImage = yield fs.existsSync(path.join(config.uploadDir, dir));

        if (!dirImage)
            yield fs.mkdir(path.join(config.uploadDir, dir));

        yield fs.writeFileAsync(newPath, newBase64String, 'base64').catch(console.error);

        try {
            const stats = yield fs.statAsync(newPath);
        } catch (ex) {
            console.error(ex);
            return false;
        }

        //const Body = fs.createReadStream(newPath);
        //const uploadResult = yield makeUploader(Body, newFilename).sendBsync();
        //yield fs.unlinkAsync(newPath);

        return '/' + dir + '/' + newFilename;

        //const validationRe = /^data:image\/(png|jpg|jpeg|gif);base64,/;
        //
        //if (!validationRe.test(base64string)) throw error('Not a valid image type', 400);
        //
        //const newBase64String = base64string.replace(/^data:image\/(png|jpg|jpeg|gif);base64,/, '');
        //const newFilename = `${prefix}_${Utils.uid(32)}_${Date.now()}.png`;
        //
        //const newPath = path.join(config.uploadDir, dir, newFilename);
        //
        //const dirImage = yield fs.existsSync(path.join(config.uploadDir, dir));
        //
        //if (!dirImage)
        //    yield fs.mkdir(path.join(config.uploadDir, dir));
        //
        //yield fs.writeFileAsync(newPath, newBase64String, 'base64').catch(console.error);
        //
        //try {
        //    const stats = yield fs.statAsync(newPath);
        //} catch (ex) {
        //    return false;
        //}
        //
        //return '/' + dir + '/' + newFilename;
    }),

    deleteImage: Async.make(function *(nameImage) {

        const imageFound = yield fs.existsSync(path.join(config.uploadDir, nameImage));

        if (!imageFound)
            return false;

        yield fs.unlinkAsync(path.join(config.uploadDir, nameImage));

        return true;
    }),
};
