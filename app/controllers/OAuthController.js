/**
 * Created by faerulsalamun on 2/16/16.
 */

'use strict';

const oauth2orize = require('oauth2orize');
const passport = require('passport');
const debug = require('debug')('app');
const Promise = require('bluebird');
const bcrypt = Promise.promisifyAll(require('bcryptjs'));
const crypto = require('crypto');
const _ = require('lodash');
const uuid = require('uuid');

// create OAuth 2.0 server
const server = oauth2orize.createServer();

// Services
const Async = require('../services/Async');
const Utils = require('../services/Utils');

// Helpers
const Email = require('../helpers/notif');

// Models
const OAuthAccessToken = require('../models/OAuthAccessToken');
const OAuthRefreshToken = require('../models/OAuthRefreshToken');
const OAuthClient = require('../models/OAuthClient');
const User = require('../models/User');

function encToken(text) {
    const encrypted = crypto.createHash('sha1').update(text).digest('hex');
    return encrypted;
}

function wrapPassword(genFunction) {
    const coroutine = Promise.coroutine(genFunction);

    return (client, username, password, scope, done) => {
        return coroutine(client, username, password, scope, done).catch(done);
    };
}

function wrapRefresh(genFunction) {
    const coroutine = Promise.coroutine(genFunction);

    return (client, refreshToken, scope, done) => {
        return coroutine(client, refreshToken, scope, done).catch(done);
    };
}

const passwordExchange = wrapPassword(function *(client, username, password, scope, done) {

    const user = yield User.findOne({ email: username });

    // Check Username
    if (_.isEmpty(user)) {
        let err = new Error('Username not found');
        err.status = 401;
        return done(err);
    }

    // Check Password
    const userPassword = _.isEmpty(user.password) ? '' : user.password;

    if (!_.isEmpty(password)) {
        const isValid = yield bcrypt.compareAsync(password, userPassword);

        if (!isValid) {
            let err = new Error('Password incorrect');
            err.status = 401;
            return done(err);
        }
    }

    // Check state
    if (!user.active) {
        let err = new Error('User not active');
        err.status = 401;
        return done(err);
    }

    // Check access token
    const accessToken = yield OAuthAccessToken.findOne({ user: user._id, client: client._id });
    const newAccessToken = Utils.uid(128);
    const newRefreshToken = Utils.uid(128);

    // if access token exists
    if (accessToken) {

        // Check refresh token
        const refreshToken = yield OAuthRefreshToken.findOne({ user: user._id, client: client._id });

        accessToken.token = encToken(newAccessToken);
        accessToken.updateExpirationDate();
        accessToken.scope = scope ? scope : '';

        refreshToken.token = encToken(newRefreshToken);

        const saveAccessToken = yield accessToken.save();
        const saveRefreshToken = yield refreshToken.save();

        return done(null, newAccessToken, newRefreshToken, { expiredin: saveAccessToken.expirationDate, user: user._id, role: user.role });
    } else {
        const accessToken = new OAuthAccessToken({
            token: encToken(Utils.uid(128)),
            client: client._id,
            user: user._id,
            scope: scope ? scope : '',
        });

        const refreshToken = new OAuthRefreshToken({
            token: encToken(Utils.uid(128)),
            client: client._id,
            user: user._id,
        });

        const saveAccessToken = yield accessToken.save();
        const saveRefreshToken = yield refreshToken.save();

        done(null, newAccessToken, newRefreshToken, { expiredIn: saveAccessToken.expirationDate, user: user._id, role: user.role });
    }
});

const refreshExchange = wrapRefresh(function *(client, refreshToken, scope, done) {
    const refreshTokenHash = encToken(refreshToken);
    const refreshTokenData = yield OAuthRefreshToken.findOne({ token: refreshTokenHash });

    if (!refreshTokenData) return done(null, false);
    if (client._id.toString() !== refreshTokenData.client.toString()) return done(null, false);

    const accessTokenData = yield OAuthAccessToken.findOne({ user: refreshTokenData.user });
    if (!accessTokenData) return done(null, false);
    if (client._id.toString() !== accessTokenData.client.toString()) return done(null, false);

    accessTokenData.token = encToken(Utils.uid(128));
    accessTokenData.updateExpirationDate();
    accessTokenData.scope = scope ? scope : '';
    if (_.isEmpty(updateAccessToken)) {
        return done(new Error('Failed to generate new access token'));
    }

    const saveAccessToken = yield accessTokenData.save();
    return done(null, accessTokenData.token, refreshToken, { expiredIn: accessTokenData.expirationDate, role: user.role });
});

//Resource owner password
server.exchange(oauth2orize.exchange.password(passwordExchange));

//Refresh Token
server.exchange(oauth2orize.exchange.refreshToken(refreshExchange));

module.exports = {
    token: [
        passport.authenticate(['clientBasic'], { session: false }),
        server.token(),
        server.errorHandler(),
    ],

    bearer: [
        passport.authenticate('accessToken', { session: false }),
    ],

    basic: passport.authenticate(['clientBasic'], { session: false }),

    getTokenDirectPassword: [
        passport.authenticate('clientPassword', { session: false }),
        server.token(),
    ],

    signup: Async.route(function *(req, res, next) {
        const missing = Utils.missingProperty(req.body, ['email', 'password', 'phone', 'fullname']);
        if (missing) {
            const err = new Error(`Missing ${missing} parameter.`);
            err.status = 400;
            return next(err);
        }

        const email = req.body.email;
        const password = req.body.password;

        const user = yield User.findOne({ email });
        if (!_.isEmpty(user)) {
            const err = new Error('Email already exist');
            err.status = 422;
            return next(err);
        }

        const newUser = {
            email,
            password: bcrypt.hashSync(password),
            phone: req.body.phone,
            fullname: req.body.fullname,
            verificationCode: uuid.v1(),
        };

        const save = yield User.create(newUser);

        Email.sendGrid(save._id, email, newUser.fullname, newUser.verificationCode, (err, resp) => {});

        return res.ok(save, 'User created', 201);
    }),

    createClientApi: Async.route(function *(req, res, next) {
        const missing = Utils.missingProperty(req.body, ['name', 'description']);
        if (missing) {
            const err = new Error(`Missing ${missing} parameter.`);
            err.status = 400;
            return next(err);
        }

        const clientData = {
            name: req.body.name,
            description: req.body.description,
            trusted: true,
        };

        const client = yield OAuthClient.create(clientData);

        res.ok(client, 'Data Client Add', 201);
    }),
};

