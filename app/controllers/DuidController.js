'use strict';

const _ = require('lodash');

// Helpers
const image = require('../helpers/image');

// Services
const Async = require('../services/Async');
const Utils = require('../services/Utils');

// Models
const Duid = require('../models/Duid');

module.exports = {

    list: Async.route(function *(req, res, next) {
        const orderBy = req.query.orderBy;
        let orderType = req.query.orderType;
        let sort = { createdTime: -1 };

        if (orderBy) {
            sort = {};
            if (!orderType) {
                orderType = 'asc';
            }

            sort[orderBy] = (orderType === 'asc') ? 1 : -1;
        }

        const data = yield Duid
                            .find()
                            .sort(sort);

        res.ok(data, 'Duid list');
    }),

    add: Async.route(function *(req, res, next) {
        const missing = Utils.missingProperty(req.body, ['title', 'jumlah']);
        if (missing) {
            const err = new Error(`Missing ${missing} parameter.`);
            err.status = 400;
            return next(err);
        }

        const data = {
            title: req.body.title,
            jumlah: req.body.jumlah,
        };

        const save = yield Duid.create(data);
        if (_.isEmpty(save)) return next(new Error('Error'));

        res.ok(save, 'Duid added');
    }),

    update: Async.route(function *(req, res, next) {
        let duid = yield Duid.findById(req.params.id);
        if (_.isEmpty(duid)) return next(new Error('Duid not found'));

        duid = _.merge(duid, req.body);
        const save = yield duid.save();

        if (_.isEmpty(save)) return next(new Error('Error'));

        res.ok(save, 'Duid updated');
    }),

    remove: Async.route(function *(req, res, next) {
        const remove = yield Duid.findByIdAndRemove(req.params.id);
        if (_.isEmpty(remove)) return next(new Error('Duid not found'));

        res.ok('Duid removed');
    }),

    detail: Async.route(function *(req, res, next) {
        const data = yield Duid.findById(req.params.id);
        if (_.isEmpty(data)) return next(new Error('Duid not found'));

        res.ok(data);
    }),
};

