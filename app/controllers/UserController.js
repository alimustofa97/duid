/**
 * Created by faerulsalamun on 2/16/16.
 */

'use strict';

const _ = require('lodash');
const bcrypt = require('bcryptjs');
const uuid = require('uuid');

// Helpers
const image = require('../helpers/image');

// Services
const Async = require('../services/Async');
const Utils = require('../services/Utils');
const Email = require('../helpers/notif');

// Config
const config = require('../../config/' + (process.env.NODE_ENV || ''));

// Models
const User = require('../models/User');
const OAuthAccessToken = require('../models/OAuthAccessToken');
const OAuthRefreshToken = require('../models/OAuthRefreshToken');

module.exports = {

    profile: Async.route(function *(req, res, next) {
        const user = yield User.findById(req.user._id).populate('admin.cabang');

        if (_.isEmpty(user)) {
            let err = new Error('User not found');
            err.status = 400;
            return next(err);
        }

        res.ok(user);
    }),

    updateProfile: Async.route(function *(req, res, next) {
        const updateAbleFields = [
            'phone',
            'fullname',
            'position',
        ];
        const update = yield User.findById(req.user._id);
        if (_.isEmpty(update)) return next(new Error('User not found'));

        const data = _.pick(req.body, updateAbleFields);
        const updateData = _.merge(update, data);
        const save = yield updateData.save(data);

        if (_.isEmpty(save)) return next(new Error('Error'));

        res.ok(save, 'Profile updated');
    }),

    updatePassword: Async.route(function *(req, res, next) {
        const user = yield User.findById(req.user._id);
        if (_.isEmpty(user)) return next(new Error('User not found'));

        const missing = Utils.missingProperty(req.body, ['password', 'confirmPassword']);
        if (missing) {
            const err = new Error(`Missing ${missing} parameter.`);
            err.status = 400;
            return next(err);
        }

        if (req.body.password !== req.body.confirmPassword) {
            return next(new Error('Password not match'));
        }

        const newPass = bcrypt.hashSync(req.body.password);
        user.password = newPass;
        const update = yield user.save();
        if (_.isEmpty(update)) return next(new Error('Error'));

        res.ok('Password updated');
    }),

    updateToken: Async.route(function *(req, res, next) {
        const user = yield User.findById(req.user._id);
        if (_.isEmpty(user)) return next(new Error('User not found'));

        const missing = Utils.missingProperty(req.body, ['token']);
        if (missing) {
            const err = new Error(`Missing ${missing} parameter.`);
            err.status = 400;
            return next(err);
        }

        user .fcmToken = req.body.token;
        const update = yield user.save();
        if (_.isEmpty(update)) return next(new Error('Error'));

        res.ok('FCM Token updated');
    }),

    verify: Async.route(function *(req, res, next) {
        const verification = req.params.code;
        const user = yield User.findOne({ verificationCode: verification });
        if (_.isEmpty(user)) return res.render('verification', { title: 'Ooops,', text: 'Your verification code invalid' });

        user.active = true;
        const update = yield user.save();
        if (_.isEmpty(update)) return next(new Error('Error'));

        res.render('verification', { title: 'Hi ' + user.fullname + ',', text: 'Congrats, your account active' });
    }),

    logout: Async.route(function *(req, res, next) {
        const removeToken = yield OAuthAccessToken.findOne({ user: req.user._id });
        if (_.isEmpty(removeToken)) return next(new Error('User not found'));

        const removedToken = yield removeToken.remove();
        if (_.isEmpty(removedToken)) return next(new Error('Error when remove token'));

        const removeRefresh = yield OAuthRefreshToken.findOne({ user: req.user._id });
        if (_.isEmpty(removeRefresh)) return next(new Error('User not found'));

        const removedRefresh = yield removeRefresh.remove();
        if (_.isEmpty(removedRefresh)) return next(new Error('Error when remove refresh'));

        res.ok('logout success');
    }),

    requestForgot: Async.route(function *(req, res, next) {
        if (!req.body.email) {
            return next(new Error('Email is required', 400));
        }

        const data = yield User.findOne({ email: req.body.email });
        if (_.isEmpty(data)) return next(new Error('Email not found', 400));

        data.forgotPassword = uuid.v1();

        const updateForgot = yield data.save();
        if (_.isEmpty(updateForgot)) return next(new Error('Failed update forgot pass', 500));

        Email.forgotPassword(data.email, updateForgot, (err, resp) => {});
        return res.ok(null, 'Email was sent');
    }),

    submitForgot: Async.route(function *(req, res, next) {
        if (!req.body.code) return next(new Error('Code is required', 400));
        if (!req.body.password) return next(new Error('Password is required', 400));

        const user = yield User.findOne({ forgotPassword: req.body.code });
        const resp = {
            title: 'Hi ' + user.fullname + ',',
            text: 'Your password has changed',
        };

        if (_.isEmpty(user)) return next(new Error('User not found', 400));

        user.password = bcrypt.hashSync(req.body.password);
        user.forgotPassword = '';

        const save = yield user.save();
        if (_.isEmpty(save)) {
            resp.title = 'Ooops,';
            resp.text = 'Something wrong,';
        }

        return res.render('verification', resp);
    }),

    forgotView: Async.route(function *(req, res, next) {
        const code = req.params.code;
        const user = yield User.findOne({ forgotPassword: code });

        const resp = {
            title: '',
            text: 'Please enter your new password',
            status: 200,
            code: '',
        };

        if (_.isEmpty(user)) {
            resp.title = 'Ooops';
            resp.text = 'Your token code invalid or expired';
            resp.status = 500;
            return res.render('verification', resp);
        } else {
            resp.title = 'Hi ' + user.fullname + ',';
            resp.code = code;
        }

        res.render('forgotPassword', resp);
    }),
};

