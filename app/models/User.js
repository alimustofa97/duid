/**
 * Created by faerulsalamun on 2/16/16.
 */

'use strict';

const mongoose = require('mongoose');
const validate = require('mongoose-validator');
const validator = validate.validatorjs;
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcryptjs');
const _ = require('lodash');
const config = require('../../config/' + (process.env.NODE_ENV || ''));
const timestamp = require('./plugins/Timestamp');
const ObjectId = mongoose.Schema.Types.ObjectId;

const emailValidator = [
    validate({
        validator: 'isEmail',
        message: 'Email is not in valid format',
        http: 400,
    }),
];

const Schema = mongoose.Schema;
const dataTables = require('mongoose-datatables');
const UserSchema = new Schema({

    email: {
        type: String,
        required: true,
        unique: true,
        validate: emailValidator,
        uniqueCaseInsensitive: true,
        lowercase: true,
    },

    password: {
        type: String,
        required: true,
    },

    forgotPassword: {
        type: String,
    },

    fullname: {
        type: String,
        default: '',
    },

    role: {
        type: [String],
        default: ['user'],
    },

    phone: {
        type: String,
    },

    active: {
        type: Boolean,
        default: false,
    },

    verificationCode: {
        type: String,
    },

    fcmToken: {
        type: String,
    },

    fcmDevice: {
        type: String,
    },

    position: {
        lat: { type: Number },
        long: { type: Number },
    },

    user: {
        level: {
            type: String,
            enum: ['bronze', 'silver', 'gold', 'platinum'],
            default: 'bronze',
        },
    },

    tukangCuci: {
        status: {
            type: String,
            default: 'notAvailable',
        },
    },

    gaji: {
        type: Number,
        default: 0,
    },

    admin: {
        cabang: {
            type: ObjectId,
            ref: 'Cabang',
        },
    },
}, { collection: config.collection.name('users'),
});

UserSchema.plugin(timestamp.useTimestamps);
UserSchema.plugin(uniqueValidator);
UserSchema.plugin(dataTables, {
    totalKey: 'recordsTotal',
    dataKey: 'data',
});

UserSchema.method('toJSON', function() {
    const user = this.toObject();

    if (_.intersection(user.role, ['user']).length === 1) {
        delete user.tukangCuci;
    }

    delete user.verificationCode;
    delete user.password;

    return user;
});

module.exports = mongoose.model('User', UserSchema);
