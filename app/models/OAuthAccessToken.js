/**
 * Created by faerulsalamun on 2/16/16.
 */

'use strict';

const mongoose = require('mongoose');
const crypto = require('crypto');
const moment = require('moment');
const _ = require('lodash');
const timestamp = require('./plugins/Timestamp');
const ObjectId = mongoose.Schema.Types.ObjectId;

const Schema = mongoose.Schema;
const OAuthAccessTokenSchema = new Schema({

    token: {
        type: String,
        required: true,
    },

    // default 6 months from now
    expirationDate: {
        type: Date,
        default: moment().add(6, 'months').toDate(),
    },

    client: {
        type: ObjectId,
        required: true,
        ref: 'OAuthClient',
    },

    user: {
        type: ObjectId,
        required: true,
        ref: 'User',
    },

    scope: {
        type: String,
        default: '',
    },

}, { collection: 'oauth_accesstoken' });

OAuthAccessTokenSchema.methods.updateExpirationDate = function() {
    this.expirationDate = moment().add(6, 'months').toDate();
    return;
};

OAuthAccessTokenSchema.plugin(timestamp.useTimestamps);
module.exports = mongoose.model('OAuthAccessToken', OAuthAccessTokenSchema);
