/**
 * Created by faerulsalamun on 5/22/16.
 */

'use strict'

const extend = require('mongoose-validator').extend;

extend('isNotNull', (val) => {
    return val === 'a';
}, 'Is not null');