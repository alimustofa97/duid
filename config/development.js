'use strict';

const _ = require('lodash');
const path = require('path');

module.exports = _.merge(require('./'), {

    // Config Server
    configServer: {
        appName: 'Duid App',
        port: process.env.PORT ? process.env.PORT : 1212,
        host: 'http://localhost',
        get urlWeb() {
            return process.env.PORT ? `http://localhost:${process.env.PORT}` : `http://localhost:${this.port}`;
        },

        get urlWebApi() {
            return this.urlWeb + '/api/v1';
        },
    },

    // database
    mongodb: {
        host: 'localhost',
        port: '27017',
        dbname: 'duid',
        username: '',
        password: '',
        get connectionUri() {
            return `mongodb://${this.username}:${this.password}@${this.host}:${this.port}/${this.dbname}`;
        },
    },

    // mongodb name collection
    collection: {
        prefix: 'duid',
        name: function(collectionName) {
            return this.prefix + '_' + collectionName;
        },
    },

    // FCM
    configFcm: {
        key: 'AIzaSyD_uW5ACJp2TDgCmBtH_OWk28w-1NlyxcU',
    },

    // Sendgrid
    sendgrid: {
        key: '_aF5INOnQdWvtODc86b5KA',
        secret: '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',
    },

    // dir
    appDir: path.join(__dirname, '..'),
    uploadDir: path.join(__dirname, '..', '/assets/upload'),

    // locale
    i18n: {
        defaultLocale: 'en_US',
    },

    // most likely should change this
    logDir: '/var/log/app',

    // swig
    swig: {
        cache: 'memory',
    },

    // nodemailer
    emailer: {
        service: 'emailService',
        user: 'username',
        pass: 'password',
    },

    cookie: {
        secret: 'BbQqBK8HEj9OrP67hkmyE9gKhPevie3q1gkIaOoUpmjvxkg6iWTkZ9HlLh6Vg7If',
    },
});
