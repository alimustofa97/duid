# Add Plat

Endpoint:
```sh
POST https://localhost:4003/api/v1/plat?access_token=ACCESS_TOKEN
```

_plat_ : required
_merk_ : optional

Request body example:
```json
{
    "plat": "D 1234 RE",
    "merk": "avanza",
}
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Plat added"
  },
  "data": {
    "__v": 0,
    "createdTime": "2016-09-04T23:30:04.197Z",
    "updatedTime": "2016-09-04T23:30:04.197Z",
    "plat": "D 1234 DE",
    "userId": "5753b7787a154d4c1c0619cf",
    "merk": "avanza",
    "_id": "57cc4c0c20ad57d027113323"
  }
}
```

# Get Plat

Endpoint:
```sh
GET https://localhost:4003/api/v1/plat?access_token=ACCESS_TOKEN&orderBy=columnName&orderType=orderType
```
_orderBy_ : Default by name
_orderType_ (asc/desc) : Default by asc

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Plat list"
  },
  "data": [
    {
      "_id": "57c31b86cebdc4ec2040f1fc",
      "createdTime": "2016-08-29T00:12:38.807Z",
      "updatedTime": "2016-08-29T00:12:38.807Z",
      "plat": "D 1234 DD",
      "merk": "avanza",
      "userId": "5753b7787a154d4c1c0619cf",
      "__v": 0
    },
    {
      "_id": "57cc4c0c20ad57d027113323",
      "createdTime": "2016-09-04T23:30:04.197Z",
      "updatedTime": "2016-09-04T23:30:04.197Z",
      "plat": "D 1234 DE",
      "userId": "5753b7787a154d4c1c0619cf",
      "merk": "avanza",
      "__v": 0
    }
  ]
}
```

# Update Plat

_UpdateAble Fields_ : plat, merk

Endpoint:
```sh
PUT https://localhost:4003/api/v1/plat/:id?access_token=ACCESS_TOKEN
```

Request body example:
```json
{
    "plat": "E 1234 ER",
    "merk": "honda"
}
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Plat updated"
  },
  "data": {
    "_id": "57cc4c0c20ad57d027113323",
    "createdTime": "2016-09-04T23:30:04.197Z",
    "updatedTime": "2016-09-04T23:32:56.391Z",
    "plat": "E 1234 ER",
    "userId": "5753b7787a154d4c1c0619cf",
    "merk": "honda",
    "__v": 0
  }
}
```

# Delete Plat

Endpoint:
```sh
DELETE https://localhost:4003/api/v1/plat/:id?access_token=ACCESS_TOKEN
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "OK"
  },
  "data": "Plat removed"
}
```


