# Add Cabang

Endpoint:
```sh
POST https://localhost:4003/api/v1/cabang?access_token=ACCESS_TOKEN
```

Request body example:
```json
{
    "name": "cabang 1",
    "position": {
        "lat": 10.00989,
        "long": 9.99087
    }
}
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Cabang added"
  },
  "data": {
    "__v": 0,
    "createdTime": "2016-05-27T11:05:05.000Z",
    "updatedTime": "2016-05-27T11:05:05.000Z",
    "name": "cabang 1",
    "_id": "5747c9accd2365083317b94e",
    "position": {
      "lat": 10.00989,
      "long": 9.99087
    },
    "listAdmin": []
  }
}
```

# Get Cabang

Endpoint:
```sh
GET https://localhost:4003/api/v1/cabang?access_token=ACCESS_TOKEN&orderBy=columnName&orderType=orderType
```
_orderBy_ : Default by name
_orderType_ (asc/desc) : Default by asc

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Cabang list"
  },
  "data": [
    {
      "_id": "5747c9accd2365083317b94e",
      "createdTime": "2016-05-27T11:05:05.000Z",
      "updatedTime": "2016-05-27T11:05:05.000Z",
      "name": "cabang 1",
      "__v": 0,
      "position": {
        "lat": 10.00989,
        "long": 9.99087
      },
      "listAdmin": []
    },
    {
      "_id": "5745887af32ac9842b04d09c",
      "createdTime": "2016-05-25T18:05:51.000Z",
      "updatedTime": "2016-05-25T18:05:51.000Z",
      "name": "new Cabang",
      "__v": 0,
      "position": {
        "lat": 10,
        "long": 20
      },
      "listAdmin": [
        {
          "userId": "57448c6d6fc3a9c4266ae67f",
          "_id": "574591c1a2e600a01c6e75d1"
        }
      ]
    }
  ]
}
```

# Update Cabang

_UpdateAble Fields_ : name, position

Endpoint:
```sh
PUT https://localhost:4003/api/v1/cabang/:id?access_token=ACCESS_TOKEN
```

Request body example:
```json
{
    "name": "cabang 11",
    "position": {
        "lat": 1,
        "long": 9
    }
}
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Cabang updated"
  },
  "data": {
    "createdTime": "2016-05-27T11:05:00.000Z",
    "updatedTime": "2016-05-27T11:05:00.000Z",
    "_id": "5747c992cd2365083317b94d",
    "name": "cabang 11",
    "__v": 0,
    "position": {
      "lat": 1,
      "long": 9
    },
    "listAdmin": []
  }
}
```

# Delete Cabang

Endpoint:
```sh
DELETE https://localhost:4003/api/v1/cabang/:id?access_token=ACCESS_TOKEN
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "OK"
  },
  "data": "Cabang removed"
}
```

# Add User In Cabang

Endpoint:
```sh
POST https://localhost:4003/api/v1/cabang/user/:id?access_token=ACCESS_TOKEN
```

Request body example:
```json
{
    "userId": "userId"
}
```

Error response, if userId is exist:
```
{
  "meta": {
    "code": 500,
    "message": "User is exist"
  }
}
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "UserId cabang added"
  },
  "data": {
    "_id": "5747c992cd2365083317b94d",
    "name": "cabang 11",
    "__v": 0,
    "createdTime": "2016-05-27T11:05:00.000Z",
    "updatedTime": "2016-05-27T11:05:00.000Z",
    "position": {
      "lat": 1,
      "long": 9
    },
    "listAdmin": [
      {
        "userId": "57448c186fc3a9c4266ae67c",
        "_id": "5747ce8acd2365083317b94f"
      }
    ]
  }
}
```

# Remove User In Cabang

Endpoint:
```sh
DELETE https://localhost:4003/api/v1/cabang/user/:id?access_token=ACCESS_TOKEN
```

Request body example:
```json
{
    "userId": "userId"
}
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "UserId cabang removed"
  },
  "data": {
    "_id": "5747c992cd2365083317b94d",
    "name": "cabang 11",
    "__v": 0,
    "createdTime": "2016-05-27T11:05:00.000Z",
    "updatedTime": "2016-05-27T11:05:00.000Z",
    "position": {
      "lat": 1,
      "long": 9
    },
    "listAdmin": [
      {
        "userId": "57448c186fc3a9c4266ae67c",
        "_id": "5747ce8acd2365083317b94f"
      }
    ]
  }
}
```
