# Order Resource

Endpoint:
```sh
GET localhost:4003/api/v1/order/resource?access_token=ACCESS_TOKEN
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Order Resource"
  },
  "data": {
    "vehicle": [
      {
        "_id": "57456ff36483a5e42cf6435f",
        "createdTime": "2016-05-25T16:05:15.000Z",
        "updatedTime": "2016-05-25T16:05:15.000Z",
        "name": "Jeep1",
        "harga": 12000,
        "__v": 0
      }
    ],
    "service": [
      {
        "_id": "5747f6b7574ed62834ad0c82",
        "createdTime": "2016-05-27T14:05:26.000Z",
        "updatedTime": "2016-05-27T14:05:26.000Z",
        "name": "Cuci2",
        "harga": 15000,
        "__v": 0
      },
      {
        "_id": "5747f5b9574ed62834ad0c81",
        "name": "Service1",
        "harga": 10000,
        "__v": 0
      },
      {
        "_id": "574be53e9dd6245421640a85",
        "createdTime": "2016-05-30T14:05:29.000Z",
        "updatedTime": "2016-05-30T14:05:29.000Z",
        "name": "jeep2",
        "harga": 1000,
        "__v": 0
      }
    ],
    "cabang": [
      {
        "_id": "577168d4592530b4010df608",
        "name": "Barossss",
        "position": {
          "lat": -6.892456293965141,
          "long": 107.53526767617188
        }
      },
    ],
    "plat": [
      {
        "_id": "57c31b86cebdc4ec2040f1fc",
        "createdTime": "2016-08-29T00:12:38.807Z",
        "updatedTime": "2016-08-29T00:12:38.807Z",
        "plat": "D 1234 DD",
        "merk": "avanza",
        "userId": "5753b7787a154d4c1c0619cf",
        "__v": 0
      },
      {
        "_id": "57c31b34cebdc4ec2040f1f7",
        "createdTime": "2016-08-29T00:11:16.150Z",
        "updatedTime": "2016-08-29T00:11:16.150Z",
        "plat": "E 1234 EE",
        "merk": "honda",
        "userId": "5753b7787a154d4c1c0619cf",
        "__v": 0
      }
    ]
  }
}
```

# Add Order

Endpoint:
```sh
POST localhost:4003/api/v1/order?access_token=ACCESS_TOKEN
```

_Promo Note_ : Before submit order, you have to check promo code with endpoint `GET /promo/check/:code`

## Order User

Request body example:
```json
{
    "vehicle": {
        "vehicleId": "57456ff36483a5e42cf6435f",
        "name": "Jeep1",
        "harga": 12000
    },
    "service": [
        {
            "serviceId": "5747f6b7574ed62834ad0c82",
            "name": "Cuci2",
            "harga": 15000
        },
        {
            "serviceId": "5747f5b9574ed62834ad0c81",
            "name": "Service1",
            "harga": 10000
        }
    ],
    "position": {
        "lat": 1000,
        "long": 5000
    },
    "address": "Jl",
    "addressNote": "Jl",
    "promoId" : "57c31a91cebdc4ec2040f1ef",
    "noPlat": {
      "platId": "57c31b86cebdc4ec2040f1fc",
      "plat": "D 1234 DD",
      "merk": "avanza"
    },
}
```

_Note_ : If user has not noPlat, you can remove `platId` parameter in `noPlat` object

## Order Washer
_Note_ : 
  - Add `user` parameter
  - Change type value of `noPlat` to String 

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Order saved"
  },
  "data": {
    "__v": 0,
    "userId": "57448c186fc3a9c4266ae67c",
    "_id": "574c1ec6885fab141fd24d2a",
    "position": {
      "lat": 1000,
      "long": 5000
    },
    "hargaTotal": 41000,
    "hargaService": 25000,
    "hargaJarak": 4000,
    "payment": "notPay",
    "status": "pending",
    "service": [
      {
        "serviceId": "5747f6b7574ed62834ad0c82",
        "name": "Cuci2",
        "harga": 15000,
        "_id": "574c1ec6885fab141fd24d2c"
      },
      {
        "serviceId": "5747f5b9574ed62834ad0c81",
        "name": "Service1",
        "harga": 10000,
        "_id": "574c1ec6885fab141fd24d2b"
      }
    ],
    "vehicle": {
      "vehicleId": "57456ff36483a5e42cf6435f",
      "name": "Jeep1",
      "harga": 12000
    }
  }
}
```

# Get Order

Endpoint:
```sh
GET localhost:4003/api/v1/order&access_token=ACCESS_TOKEN
```

Request Query : 
_limit_ : number,
_skip_ : number,
_status_ : status

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "User Order"
  },
  "data": [
    {
      "_id": "574ab8dd8c0480541c5a57c0",
      "createdTime": "2016-05-29T16:05:10.000Z",
      "updatedTime": "2016-05-29T16:05:10.000Z",
      "userId": "57448c186fc3a9c4266ae67c",
      "__v": 0,
      "position": {
        "lat": 1000,
        "long": 5000
      },
      "hargaTotal": 41000,
      "hargaService": 25000,
      "hargaJarak": 4000,
      "payment": "notPay",
      "status": "pending",
      "service": [
        {
          "serviceId": "5747f6b7574ed62834ad0c82",
          "name": "Cuci2",
          "harga": 15000,
          "_id": "574ab8dd8c0480541c5a57c2"
        },
        {
          "serviceId": "5747f5b9574ed62834ad0c81",
          "name": "Service1",
          "harga": 10000,
          "_id": "574ab8dd8c0480541c5a57c1"
        }
      ],
      "vehicle": {
        "vehicleId": "57456ff36483a5e42cf6435f",
        "name": "Jeep1",
        "harga": 12000
      }
    },
  ]
}
```

# Update Order

Endpoint:
```sh
PUT localhost:4003/api/v1/order/574ab8dd8c0480541c5a57c0?access_token=ACCESS_TOKEN
```

_updateAblefields_ : sales, tukangCuci, status(pending, onProcess, onGoing, done), payment(notPay, paid)

Request body example:
```json
{
    "status": "onProcess"
}
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Status changed"
  },
  "data": {
    "_id": "574ab8dd8c0480541c5a57c0",
    "createdTime": "2016-05-29T16:05:10.000Z",
    "updatedTime": "2016-05-30T18:05:35.000Z",
    "userId": "57448c186fc3a9c4266ae67c",
    "__v": 0,
    "position": {
      "lat": 1000,
      "long": 5000
    },
    "hargaTotal": 41000,
    "hargaService": 25000,
    "hargaJarak": 4000,
    "payment": "notPay",
    "status": "onProcess",
    "service": [
      {
        "serviceId": "5747f6b7574ed62834ad0c82",
        "name": "Cuci2",
        "harga": 15000,
        "_id": "574ab8dd8c0480541c5a57c2"
      },
      {
        "serviceId": "5747f5b9574ed62834ad0c81",
        "name": "Service1",
        "harga": 10000,
        "_id": "574ab8dd8c0480541c5a57c1"
      }
    ],
    "vehicle": {
      "vehicleId": "57456ff36483a5e42cf6435f",
      "name": "Jeep1",
      "harga": 12000
    }
  }
}
```

# Get All Order

Endpoint:
```sh
GET localhost:4003/api/v1/order/all&access_token=ACCESS_TOKEN
```

Request Query : 
_orderBy_ : column name,
_orderType_ : desc/asc

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "User Order"
  },
  "data": [
    {
      "_id": "574ab8dd8c0480541c5a57c0",
      "createdTime": "2016-05-29T16:05:10.000Z",
      "updatedTime": "2016-05-29T16:05:10.000Z",
      "userId": "57448c186fc3a9c4266ae67c",
      "__v": 0,
      "position": {
        "lat": 1000,
        "long": 5000
      },
      "hargaTotal": 41000,
      "hargaService": 25000,
      "hargaJarak": 4000,
      "payment": "notPay",
      "status": "pending",
      "service": [
        {
          "serviceId": "5747f6b7574ed62834ad0c82",
          "name": "Cuci2",
          "harga": 15000,
          "_id": "574ab8dd8c0480541c5a57c2"
        },
        {
          "serviceId": "5747f5b9574ed62834ad0c81",
          "name": "Service1",
          "harga": 10000,
          "_id": "574ab8dd8c0480541c5a57c1"
        }
      ],
      "vehicle": {
        "vehicleId": "57456ff36483a5e42cf6435f",
        "name": "Jeep1",
        "harga": 12000
      }
    },
  ]
}
```