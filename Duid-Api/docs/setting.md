# Add Setting

Endpoint:
```sh
POST https://localhost:4003/api/v1/setting?access_token=ACCESS_TOKEN
```

Request body example:
```json
{
    "name": "tes",
    "value": 5000
}
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Setting added"
  },
  "data": {
    "__v": 0,
    "name": "tes",
    "_id": "574c230265ed6c004eb221f4",
    "value": 5000
  }
}
```

# Get Setting

Endpoint:
```sh
GET https://localhost:4003/api/v1/setting?access_token=ACCESS_TOKEN
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Setting Data"
  },
  "data": [
    {
      "_id": "5748017dfd2407c050f68c77",
      "createdTime": "2016-05-27T15:05:37.000Z",
      "updatedTime": "2016-05-27T15:05:37.000Z",
      "name": "hargaJarak",
      "__v": 0,
      "value": 1000
    }
  ]
}
```

# Update Setting

_UpdateAble Fields_ : name, value

Endpoint:
```sh
PUT https://localhost:4003/api/v1/setting/:id?access_token=ACCESS_TOKEN
```

Request body example:
```json
{
    "name": "tes1",
    "value": 1000
}
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Setting updated"
  },
  "data": {
    "_id": "574c230265ed6c004eb221f4",
    "name": "tes1",
    "__v": 0,
    "createdTime": "2016-05-30T18:05:11.000Z",
    "updatedTime": "2016-05-30T18:05:11.000Z",
    "value": 1000
  }
}
```

# Delete Setting

Endpoint:
```sh
DELETE https://localhost:4003/api/v1/setting/:id?access_token=ACCESS_TOKEN
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "OK"
  },
  "data": "Setting removed"
}
```


